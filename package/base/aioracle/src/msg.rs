use cosmwasm_std::HumanAddr;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

use crate::state::AIRequest;

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, JsonSchema)]
pub struct InitMsg {
    pub dsources: Vec<HumanAddr>,
    pub tcases: Vec<HumanAddr>,
    pub threshold: u8,
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, JsonSchema)]
#[serde(rename_all = "snake_case")]
pub enum HandleMsg {
    SetDataSources {
        dsources: Vec<HumanAddr>,
    },
    SetTestCases {
        tcases: Vec<HumanAddr>,
    },
    SetValidatorFees {
        fees: u64,
    },
    CreateAiRequest(AIRequestMsg),
    // all logics must go through Oracle AI module instead of smart contract to avoid gas price problem
    Aggregate {
        dsource_results: Vec<String>,
        request_id: u64,
    },
    SetThreshold(u8),
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, JsonSchema)]
pub struct AIRequestMsg {
    pub validators: Vec<HumanAddr>,
    pub input: String,
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, JsonSchema)]
#[serde(rename_all = "snake_case")]
pub enum QueryMsg {
    Get {
        dsource: HumanAddr,
        input: String,
    },
    Test {
        dsource: HumanAddr,
        input: String,
        output: String,
    },
    GetDataSources {},
    GetTestCases {},
    GetThreshold {},
    GetRequest {
        request_id: u64,
    },
    GetRequests {
        offset: Option<u64>,
        limit: Option<u8>,
        order: Option<u8>,
    },
    GetMinFees {
        validators: Vec<HumanAddr>,
    },
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, JsonSchema)]
pub struct AIRequestsResponse {
    pub items: Vec<AIRequest>,
    pub total: u64,
}

// for query other contract
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, JsonSchema)]
#[serde(rename_all = "snake_case")]
pub enum DataSourceQueryMsg {
    Get { input: String },
    GetFees {},
}
